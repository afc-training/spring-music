#!/bin/bash -x

version=1.0.0
REGISTRY_IMAGE="${EXT_REGISTRY_IMAGE:-$CI_REGISTRY_IMAGE}"

echo "${REGISTRY_IMAGE}/${CI_COMMIT_BRANCH}:latest" \
| kbld -f - --imgpkg-lock-output common/.imgpkg/images.yml

imgpkg push \
  -b ${REGISTRY_IMAGE}/${CI_PROJECT_NAME}-imgpkg/${CI_PROJECT_NAME}:${version} \
  -f common

imgpkg copy \
  -b ${REGISTRY_IMAGE}/${CI_PROJECT_NAME}-imgpkg/${CI_PROJECT_NAME}:${version} \
  --to-repo ${REGISTRY_IMAGE}/${CI_PROJECT_NAME}-imgpkg-bundle/${CI_PROJECT_NAME}